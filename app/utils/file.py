import os
from typing import List

KB = 1024
MB = 1024 * KB
GB = 1024 * MB
IMG_SIZE = 300 * KB
IMG_PER_RECORD = 4


def create_dir_if_not_exist(path):
    isExist = os.path.exists(path)
    if not isExist:
        os.makedirs(path)


def get_file_extension(path):
    return path.split(".")[-1]


def valid_file_extension(filename: str, extensions: List[str]):
    for extension in extensions:
        if str(filename).endswith(extension):
            return True
    return False


def get_folder_size(root_path):
    size = 0
    for path, dirs, files in os.walk(root_path):
        for f in files:
            fp = os.path.join(path, f)
            size += os.path.getsize(fp)
    return round(size / GB, 3)


if __name__ == "__main__":
    path = 'C:\\TSP\\device-gateway\\.env'
    print(get_file_extension(path))
