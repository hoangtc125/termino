from typing import TypeVar
from pydantic import BaseModel

T = TypeVar("T")
Src = TypeVar("Src")


def get_dict(object: T, allow_none=False):
    """
        chuyển thông tin từ object sang dict
    """
    res = {}
    data = object if type(object) is dict else object.__dict__
    for key, value in data.items():
        if value is None and allow_none is False:
            continue
        if isinstance(value, BaseModel):
            res[key] = get_dict(value)
        elif type(value) is not dict:
            res[key] = value
        elif len(value.keys()) == 0:
            continue
        else:
            res[key] = get_dict(value, allow_none)
    return res
