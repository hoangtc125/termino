import time
from datetime import datetime
from typing import Optional

from dateutil.relativedelta import relativedelta


def get_current_timestamp() -> int:
    return int(datetime.now().timestamp())


def get_timestamp_after(current_time: Optional[int] = None, **args):
    current_datetime = datetime.fromtimestamp(current_time) if current_time else datetime.now()
    datetime_after = current_datetime + relativedelta(**args)
    return int(datetime_after.timestamp())


def creationDate_to_timestamp(creationDate: str):
    time_object = datetime.strptime(creationDate.split(".")[0], "%Y%m%d%H%M%S")
    timestamp = time_object.timestamp()
    return int(timestamp)


if __name__ == "__main__":
    print(creationDate_to_timestamp("20240126204202"))
