from fastapi import APIRouter, Depends

from app.infra.api_config import ServiceAPI
from app.infra.http_resp import success_response
from app.core.service_manager import IServiceManager
from .dependencies import get_service_manager
from app.adapter.prometheus import PrometheusTemplateModel
from app.adapter.monitor_rule import MonitorRuleTemplateModel

tags = ["service-prometheus"]
router = APIRouter()


@router.get(ServiceAPI.PROMETHEUS_RULE_GET, tags=tags)
def get_rule(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["prometheus"].get_rule_config()
    return success_response(data=resp)


@router.get(ServiceAPI.PROMETHEUS_CONFIG_GET, tags=tags)
def get_config(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["prometheus"].get_config()
    return success_response(data=resp)


@router.put(ServiceAPI.PROMETHEUS_RULE_UPDATE, tags=tags)
def update_rule(
        data: MonitorRuleTemplateModel,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["prometheus"].update_rule_config(data)
    serviceManager.services["prometheus"].restart()
    return success_response(data=resp)


@router.put(ServiceAPI.PROMETHEUS_CONFIG_UPDATE, tags=tags)
def update_config(
        data: PrometheusTemplateModel,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["prometheus"].update_config(data)
    serviceManager.services["prometheus"].restart()
    return success_response(data=resp)
