from fastapi import APIRouter, Depends

from app.infra.api_config import ServiceAPI
from app.infra.http_resp import success_response
from app.core.service_manager import IServiceManager
from .dependencies import get_service_manager
from app.adapter.alertmanager import AlertManagerTemplateModel

tags = ["service-alertmanager"]
router = APIRouter()


@router.get(ServiceAPI.ALERT_RECEIVER_GET, tags=tags)
def get_receiver(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["alert_manager"].get_receiver()
    return success_response(data=resp)


@router.put(ServiceAPI.ALERT_RECEIVER_UPDATE, tags=tags)
def update_receiver(
        data: AlertManagerTemplateModel,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    resp = serviceManager.services["alert_manager"].update_receiver(data)
    serviceManager.services["alert_manager"].restart()
    return success_response(data=resp)
