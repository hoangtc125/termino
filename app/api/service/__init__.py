from .control.route import router as service_control_router
from .info.route import router as service_info_router
from .prometheus.route import router as service_prometheus_router
from .alertmanager.route import router as service_alertmanager_router
