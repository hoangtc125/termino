from fastapi import APIRouter, Depends

from app.infra.api_config import ServiceAPI
from app.infra.http_resp import success_response
from app.infra.http_exc import CustomHTTPException
from app.core.service_manager import IServiceManager
from .dependencies import get_service_manager

tags = ["service-info"]
router = APIRouter()


@router.get(ServiceAPI.INFO_STATUS, tags=tags)
def get_status(
        service: str,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    if service and service in serviceManager.services.keys():
        return success_response(data=serviceManager.services[service].is_running())
    else:
        raise CustomHTTPException(error_type="service_not_exist")


@router.get(ServiceAPI.INFO_STATUS_ALL, tags=tags)
def get_status_all(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    return success_response(data=serviceManager.is_running())
