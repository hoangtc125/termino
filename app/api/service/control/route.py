from fastapi import APIRouter, Depends

from app.infra.api_config import ServiceAPI
from app.infra.http_resp import success_response
from app.infra.http_exc import CustomHTTPException
from app.core.service_manager import IServiceManager
from .dependencies import get_service_manager

tags = ["service-control"]
router = APIRouter()


@router.on_event("startup")
def create_service_manager():
    get_service_manager()


@router.post(ServiceAPI.CONTROL_START, tags=tags)
def start(
        service: str,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    """
        api dùng để bật dịch vụ
    """
    if service in serviceManager.services.keys():
        serviceManager.stateAdapter.enable(service)
        resp = serviceManager.services[service].start()
        return success_response(resp)
    else:
        raise CustomHTTPException(error_type="service_not_exist")


@router.post(ServiceAPI.CONTROL_STOP, tags=tags)
def stop(
        service: str,
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    """
        api dùng để tắt dịch vụ
    """
    if service in serviceManager.services.keys():
        serviceManager.stateAdapter.disable(service)
        resp = serviceManager.services[service].stop()
        return success_response(resp)
    else:
        raise CustomHTTPException(error_type="service_not_exist")


@router.post(ServiceAPI.CONTROL_START_ALL_MONITOR, tags=tags)
async def start_all_monitor(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    serviceManager.stateAdapter.enable_all()
    await serviceManager.start_all()
    return success_response()


@router.post(ServiceAPI.CONTROL_STOP_ALL_MONITOR, tags=tags)
def stop_all_monitor(
        serviceManager: IServiceManager = Depends(get_service_manager),
):
    serviceManager.stateAdapter.disable_all()
    serviceManager.stop_all()
    return success_response()
