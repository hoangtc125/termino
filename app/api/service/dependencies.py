from app.core.service_manager import IServiceManager, ServiceManager


def get_service_manager() -> IServiceManager:
    return ServiceManager()
