from fastapi import APIRouter, Depends

from app.infra.api_config import SystemAPI
from app.infra.http_resp import success_response
from app.core.system import ISystem
from .dependencies import get_system_service

tags = ["system"]
router = APIRouter()


@router.get(SystemAPI.LANGUAGE_GET, tags=tags)
def get_language(
        systemService: ISystem = Depends(get_system_service),
):
    resp = systemService.systemAdapter.data.language
    return success_response(data=resp)


@router.post(SystemAPI.LANGUAGE_UPDATE, tags=tags)
def update_language(
        language: str,
        systemService: ISystem = Depends(get_system_service),
):
    systemService.systemAdapter.edit_language(language)
    return success_response()
