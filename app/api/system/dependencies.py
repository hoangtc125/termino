from app.core.system import ISystem, SystemService


def get_system_service() -> ISystem:
    return SystemService()
