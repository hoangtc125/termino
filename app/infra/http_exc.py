from fastapi import HTTPException, status

from .config import response_code


class CustomHTTPException(HTTPException):
    def __init__(
            self,
            error_type,
            message=None,
            headers={"WWW-Authenticate": "Bearer"},
            status_code=status.HTTP_400_BAD_REQUEST
    ):
        CustomHTTPException.error_code = response_code["error"][error_type]["code"]
        CustomHTTPException.error_message = message if message else response_code["error"][error_type]["message"]
        CustomHTTPException.headers = headers
        CustomHTTPException.status_code = status_code
