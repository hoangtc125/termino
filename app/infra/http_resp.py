import json
from typing import Optional, TypeVar
from pydantic import BaseModel

from .config import response_code

# danh sách mã lỗi được định nghĩa trong file

T = TypeVar("T")


class HttpResponse(BaseModel):
    status_code = response_code["success"]["code"]
    msg = response_code["success"]["message"]
    data: Optional[T] = None


def custom_response(status_code, message: str, data: T) -> HttpResponse:
    return HttpResponse(status_code=status_code, msg=message, data=data)


def success_response(data=None):
    return HttpResponse(status_code=200, msg="Success", data=data)
