import json
import os
from dotenv import load_dotenv
from pydantic import BaseModel, BaseSettings

DASHBOARD_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))
load_dotenv(os.path.join(DASHBOARD_DIR, ".env"))

f_json = open(DASHBOARD_DIR + r"/resources/response_code.json", encoding="utf8")
response_code = json.load(f_json)


class Settings(BaseSettings):
    PROJECT_NAME = os.getenv("PROJECT_NAME", "DASHBOARD MONITOR")
    API_PREFIX = ""
    DASHBOARD_CORS_ORIGINS = ["*"]
    DASHBOARD_MONITOR_PORT = os.getenv("DASHBOARD_MONITOR_PORT", 9081)
    FRONTEND_MONITOR_PORT = os.getenv("FRONTEND_MONITOR_PORT", 9082)
    TIME_PROVISION = os.getenv("TIME_PROVISION", 10)
    TIME_START_RELAX = os.getenv("TIME_START_RELAX", 0.5)
    UI_DIR = os.getenv("UI_DIR", DASHBOARD_DIR + r'\build')


settings = Settings()

print(settings)
