class ServiceAPI:
    CONTROL_START = "/service/{service}/control/start"
    CONTROL_STOP = "/service/{service}/control/stop"
    CONTROL_START_ALL_MONITOR = "/service/control/monitor/start"
    CONTROL_STOP_ALL_MONITOR = "/service/control/monitor/stop"
    INFO_STATUS = "/service/{service}/info/status"
    INFO_STATUS_ALL = "/service/info/status"
    PROMETHEUS_RULE_GET = "/service/prometheus/rule/get"
    PROMETHEUS_RULE_UPDATE = "/service/prometheus/rule/update"
    PROMETHEUS_CONFIG_GET = "/service/prometheus/config/get"
    PROMETHEUS_CONFIG_UPDATE = "/service/prometheus/config/update"
    ALERT_RECEIVER_GET = "/service/alertmanager/receiver/get"
    ALERT_RECEIVER_UPDATE = "/service/alertmanager/receiver/update"

class SystemAPI:
    LANGUAGE_UPDATE = "/system/language/update/{language}"
    LANGUAGE_GET = "/system/language/get"

def get_api(API):
    apis = []
    for value in API.__dict__.values():
        if isinstance(value, str) and str(value).startswith("/"):
            apis.append(value)
    return apis


APIs = [
    *get_api(ServiceAPI),
    *get_api(SystemAPI),
]

WHITE_LIST = [
    SystemAPI.LANGUAGE_GET,
    SystemAPI.LANGUAGE_UPDATE,
]

if __name__ == "__main__":
    print(APIs)
