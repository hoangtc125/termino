import grequests  # DON'T REMOVE
import uvicorn
import time
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.openapi.docs import (
    get_redoc_html,
    get_swagger_ui_html,
    get_swagger_ui_oauth2_redirect_html,
)
from fastapi.staticfiles import StaticFiles

from app.infra.http_exc import CustomHTTPException
from app.infra.config import settings, DASHBOARD_DIR
from app.api.service import service_control_router, service_info_router, service_prometheus_router, \
    service_alertmanager_router
from app.api.system import system_language_router

# khởi tạo server không có swagger
app = FastAPI(version=1.0, docs_url=None, redoc_url=None)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# tạo swagger cho server
app.mount("/static", StaticFiles(directory=DASHBOARD_DIR + r"/static"), name="static")


@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=app.title + " - Swagger UI",
        oauth2_redirect_url=app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
    )


@app.get(app.swagger_ui_oauth2_redirect_url, include_in_schema=False)
async def swagger_ui_redirect():
    return get_swagger_ui_oauth2_redirect_html()


@app.get("/redoc", include_in_schema=False)
async def redoc_html():
    return get_redoc_html(
        openapi_url=app.openapi_url,
        title=app.title + " - ReDoc",
        redoc_js_url="/static/redoc.standalone.js",
    )


# middleware để kiểm tra jwt, license
@app.middleware("http")
async def add_request_middleware(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    response.headers["X-process-time"] = str(time.time() - start_time)
    return response


# middleware xử lý ngoại lệ
@app.exception_handler(CustomHTTPException)
async def uvicorn_exception_handler(request: Request, exc: CustomHTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content={
            "status_code": exc.error_code,
            "msg": exc.error_message
        }
    )


app.include_router(service_control_router)
app.include_router(service_info_router)
app.include_router(service_prometheus_router)
app.include_router(service_alertmanager_router)

app.include_router(system_language_router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(settings.DASHBOARD_MONITOR_PORT))
