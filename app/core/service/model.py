from typing import List
from pydantic import BaseModel


class ServiceGroup:
    REQUIRED = "required"
    AT_LEAST_ONE = "at_least_one"
    OPTIONAL = "optional"


class ServiceDepend(BaseModel):
    key: str
    time: int = None


class ServiceRelation(BaseModel):
    group: str
    depend: List[ServiceDepend] = []


class ServiceCMD(BaseModel):
    pre: List[str] = []
    post: List[str] = []


class ServiceModel(BaseModel):
    name: str
    key: str
    relation: ServiceRelation
    dir_location: str
    script_location: str
    executable_path: str
    log_location: str = None
    env_location: str = None
    config_location: str = None
    host: str = ""
    cmd = ServiceCMD()
