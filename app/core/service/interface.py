import os
import re
import subprocess
import traceback
import psutil
from typing import Dict, Any


from app.adapter.state import IStateAdapter
from app.adapter.system import ISystemAdapter
from app.utils.time import creationDate_to_timestamp
from .model import ServiceModel


class IService:
    def __init__(self, info: ServiceModel, stateAdapter: IStateAdapter, systemAdapter: ISystemAdapter):
        self.info = info
        self.stateAdapter = stateAdapter
        self.systemAdapter = systemAdapter
        self.env: Dict[str, Any] = {}

    def start(self):
        """
            hàm thực thi file exe dựa vào đường dẫn sau đó lưu lại state
        """
        if self.is_running():
            return True
        try:
            for cmd in self.info.cmd.pre:
                print(f"Pre: {cmd}")
                subprocess.Popen(cmd.split(), creationflags=subprocess.CREATE_NO_WINDOW).wait()
            print(f"Start: {self.info.name}")
            pid = subprocess.Popen(
                self.info.script_location.split(),
                creationflags=subprocess.CREATE_NO_WINDOW,
                shell=True,
            ).pid
            self.stateAdapter.edit_service_pid(self.info.key, pid)
            for cmd in self.info.cmd.post:
                print(f"Post: {cmd}")
                subprocess.Popen(cmd.split(), creationflags=subprocess.CREATE_NO_WINDOW).wait()
            return True
        except Exception as e:
            print(e)
            return False

    def stop(self):
        """
            hàm tắt dịch vụ dựa vào đường dẫn và lưu lại state
        """
        if not self.is_running():
            return True
        try:
            print(f"Stop: {self.info.name}")
            parent = psutil.Process(int(self.stateAdapter.data.services[self.info.key].pid))
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()
            self.stateAdapter.edit_service_pid(self.info.key, None)
            return True
        except Exception as e:
            traceback.print_exc()
            return False

    def restart(self):
        self.stop()
        self.start()

    def is_running(self):
        """
            nếu chưa có pid của dịch vụ, sẽ phải tìm pid và kiểm tra trạng thái dịch vụ dựa vào pid đó
        """
        pid = self.get_pid()
        if self.stateAdapter.data.services[self.info.key].pid != pid:
            self.stateAdapter.edit_service_pid(self.info.key, pid)
            port = self.get_port_by_pid(pid)
            self.stateAdapter.edit_service_port(self.info.key, port)
        if not pid:
            return False
        return True

    def get_pid(self):
        """
            hàm lấy pid của dịch vụ dựa vào đường dẫn
        """
        try:
            path = os.path.normpath(self.info.executable_path).replace("\\", "\\\\")
            cargs = (
                "wmic", "process", "where", f"ExecutablePath like \'%{path}%\'",
                "get", "ProcessId",
                "|", "findstr", "/v", "ProcessId",
                "|", "findstr", "/v", "wmic",
            )
            pid = subprocess.check_output(cargs, stderr=subprocess.PIPE, shell=True, encoding="utf-8").strip()
            if pid.isdigit():
                return int(pid)
            return None
        except:
            return self.stateAdapter.data.services[self.info.key].pid

    def get_creationDate(self):
        """
            hàm lấy creationDate của dịch vụ dựa vào đường dẫn
        """
        try:
            path = os.path.normpath(self.info.executable_path).replace("\\", "\\\\")
            cargs = (
                "wmic", "process", "where", f"ExecutablePath like \'%{path}%\'",
                "get", "CreationDate",
                "|", "findstr", "/v", "CreationDate",
                "|", "findstr", "/v", "wmic",
            )
            creation_date = subprocess.check_output(cargs, stderr=subprocess.PIPE, shell=True, encoding="utf-8").strip()
            return creationDate_to_timestamp(creation_date)
        except:
            return None

    def get_port_by_pid(self, pid: str):
        """
            hàm lấy creationDate của dịch vụ dựa vào đường dẫn
        """
        try:
            cargs = (
                "netstat", "-ano",
                "|", "findstr", str(pid),
            )
            hosts = subprocess.check_output(cargs, stderr=subprocess.PIPE, shell=True, encoding="utf-8").strip()
            pattern = re.compile(r":(\d+)")
            ports = pattern.findall(hosts)
            for port in ports:
                if port.isdigit() and int(port) != 0:
                    return int(port)
        except:
            return None
