from ..interface import IService
from .model import WindowsExporterModel
from app.adapter.state import IStateAdapter
from app.adapter.system import ISystemAdapter


class WindowsExporterService(IService):
    def __init__(self, info: WindowsExporterModel, stateAdapter: IStateAdapter, systemAdapter: ISystemAdapter):
        super().__init__(info, stateAdapter, systemAdapter)
