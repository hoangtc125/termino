import os
from typing import Dict

import yaml

from ..interface import IService
from .model import PrometheusModel
from app.infra.config import DASHBOARD_DIR
from app.adapter.state import IStateAdapter
from app.adapter.system import ISystemAdapter
from app.adapter.monitor_rule import MonitorRuleTemplateModel, MonitorRuleTemplate, ImonitorRuleTemplate
from app.adapter.prometheus import PrometheusTemplate, PrometheusTemplateModel, IPrometheusTemplate


class PrometheusService(IService):
    def __init__(self, info: PrometheusModel, stateAdapter: IStateAdapter, systemAdapter: ISystemAdapter):
        super().__init__(info, stateAdapter, systemAdapter)
        self.rule_template: ImonitorRuleTemplate = MonitorRuleTemplate()
        self.config_template: IPrometheusTemplate = PrometheusTemplate()

    def start(self):
        os.chdir(self.info.dir_location)
        super().start()
        os.chdir(DASHBOARD_DIR)

    def get_rule_config(self):
        with open(self.info.rule_location, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            return MonitorRuleTemplateModel(
                down_duration=data["groups"][0]["rules"][0]["for"],
                high_memory_threshold=data["groups"][0]["rules"][1]["expr"].split("<")[-1].strip(),
                high_memory_duration=data["groups"][0]["rules"][1]["for"],
                low_disk_threshold=data["groups"][0]["rules"][2]["expr"].split("<")[-1].strip(),
                low_disk_duration=data["groups"][0]["rules"][2]["for"],
            )

    def get_config(self):
        with open(self.info.config_location, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            return PrometheusTemplateModel(
                seed_hosts=data["scrape_configs"][0]["static_configs"][0]["targets"]
            )

    def update_rule_config(self, data: MonitorRuleTemplateModel):
        parse_data = self.rule_template.parse_data(data)
        yaml.Dumper.ignore_aliases = lambda self, data: True
        with open(self.info.rule_location, "w", encoding="utf8") as yaml_file:
            yaml.dump(parse_data, yaml_file, default_flow_style=False, Dumper=yaml.Dumper)

    def update_config(self, data: PrometheusTemplateModel):
        parse_data = self.config_template.parse_data(data)
        yaml.Dumper.ignore_aliases = lambda self, data: True
        with open(self.info.config_location, "w", encoding="utf8") as yaml_file:
            yaml.dump(parse_data, yaml_file, default_flow_style=False, Dumper=yaml.Dumper)
