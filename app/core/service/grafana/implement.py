import os

from ..interface import IService
from .model import GrafanaModel
from app.infra.config import DASHBOARD_DIR
from app.adapter.state import IStateAdapter
from app.adapter.system import ISystemAdapter


class GrafanaService(IService):
    def __init__(self, info: GrafanaModel, stateAdapter: IStateAdapter, systemAdapter: ISystemAdapter):
        super().__init__(info, stateAdapter, systemAdapter)

    def start(self):
        os.chdir(self.info.dir_location)
        super().start()
        os.chdir(DASHBOARD_DIR)
