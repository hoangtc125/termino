import os

import yaml

from ..interface import IService
from .model import AlertManagerModel
from app.infra.config import DASHBOARD_DIR
from app.adapter.state import IStateAdapter
from app.adapter.system import ISystemAdapter
from app.adapter.alertmanager import AlertManagerTemplateModel, IAlertManagerTemplate, AlertManagerTemplate


class AlertManagerService(IService):
    def __init__(self, info: AlertManagerModel, stateAdapter: IStateAdapter, systemAdapter: ISystemAdapter):
        super().__init__(info, stateAdapter, systemAdapter)
        self.alert_manager_template: IAlertManagerTemplate = AlertManagerTemplate()

    def start(self):
        os.chdir(self.info.dir_location)
        super().start()
        os.chdir(DASHBOARD_DIR)

    def get_receiver(self):
        with open(self.info.config_location, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            return AlertManagerTemplateModel(**{"receivers": data["receivers"]})

    def update_receiver(self, data: AlertManagerTemplateModel):
        parse_data = self.alert_manager_template.parse_data(data)
        yaml.Dumper.ignore_aliases = lambda self, data: True
        with open(self.info.config_location, "w", encoding="utf8") as yaml_file:
            yaml.dump(parse_data, yaml_file, default_flow_style=False, Dumper=yaml.Dumper)
