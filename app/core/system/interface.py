from app.adapter.system import SystemAdapter, ISystemAdapter
from .model import CloudSyncData


class ISystem:
    def __init__(self):
        self.systemAdapter: ISystemAdapter = SystemAdapter()
