from typing import Optional

from pydantic import BaseModel


class DeviceHealth(BaseModel):
    ip: str
    port: Optional[str] = None
    name: Optional[str] = None
    status: Optional[bool] = None


class CloudSyncData(BaseModel):
    api_key: str
    base_url: str
    interval: int
    status: bool
