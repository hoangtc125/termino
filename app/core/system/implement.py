from app.lib import singleton
from .interface import ISystem


@singleton
class SystemService(ISystem):
    def __init__(self):
        super().__init__()
