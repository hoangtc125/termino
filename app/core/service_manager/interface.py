from app.core.service import IService
from app.adapter.service import ServiceAdapter
from app.adapter.state import StateAdapter
from app.adapter.system import SystemAdapter
from typing import Dict


class IServiceManager:
    def __init__(self):
        self.systemAdapter = SystemAdapter()
        self.serviceAdapter = ServiceAdapter()
        self.stateAdapter = StateAdapter(self.serviceAdapter)  # common state adapter
        self.services: Dict[str, IService] = {}
        self.is_valid_dependencies = True

    def bootstrap(self):
        raise NotImplementedError()

    def valid_dependencies(self, service: str, visited=None):
        raise NotImplementedError()

    async def safe_start(self, service: str):
        raise NotImplementedError()

    async def start_all(self):
        raise NotImplementedError()

    def stop_all(self):
        raise NotImplementedError()

    def is_running(self):
        raise NotImplementedError()

    def provision(self):
        raise NotImplementedError()
