import grequests
import asyncio
import threading
import time
from typing import List

from app.infra.config import settings
from app.infra.http_exc import CustomHTTPException
from app.core.service_manager.interface import IServiceManager
from app.core.service.grafana import GrafanaService, GrafanaModel
from app.core.service.prometheus import PrometheusService, PrometheusModel
from app.core.service.alert_manager import AlertManagerService, AlertManagerModel
from app.core.service.windows_exporter import WindowsExporterService, WindowsExporterModel
from app.lib import singleton
from app.utils.time import get_current_timestamp
from .model import ServiceStatusResponse


@singleton
class ServiceManager(IServiceManager):
    def __init__(self):
        super().__init__()
        self.bootstrap()

    def bootstrap(self):
        for key, service in self.serviceAdapter.data.items():
            if key == "grafana":
                service_info = GrafanaModel(**service)
                self.services[key] = GrafanaService(service_info, self.stateAdapter, self.systemAdapter)
            elif key == "prometheus":
                service_info = PrometheusModel(**service)
                self.services[key] = PrometheusService(service_info, self.stateAdapter, self.systemAdapter)
            elif key == "alert_manager":
                service_info = AlertManagerModel(**service)
                self.services[key] = AlertManagerService(service_info, self.stateAdapter, self.systemAdapter)
            elif key == "windows_exporter":
                service_info = WindowsExporterModel(**service)
                self.services[key] = WindowsExporterService(service_info, self.stateAdapter, self.systemAdapter)
        for service in self.services.keys():
            if not self.valid_dependencies(service):
                self.is_valid_dependencies = False
                break
        provision_thread = threading.Thread(target=self.provision, args=())
        provision_thread.daemon = True
        provision_thread.start()

    def valid_dependencies(self, service: str, visited=None):
        if visited is None:  # Nếu visited chưa được khởi tạo, khởi tạo nó là một tập hợp rỗng
            visited = set()
        if service in visited:  # Nếu service_key đã được thăm trước đó, có vòng lặp, trả về False
            return False
        visited.add(service)  # Đánh dấu service đã được thăm
        # Kiểm tra dependences của mỗi service phụ thuộc
        for depend in self.services[service].info.relation.depend:
            if not self.valid_dependencies(depend.key, visited):
                return False
        # Loại bỏ service khỏi tập hợp visited để sử dụng cho các lần gọi sau
        visited.remove(service)
        return True

    async def safe_start(self, service: str):
        if self.stateAdapter.data.services[service].pid:  # already running:
            return
        if self.is_valid_dependencies:
            for depend in self.services[service].info.relation.depend:
                if self.stateAdapter.data.services[depend.key].pid:  # already running
                    created_at = self.services[depend.key].get_creationDate()
                    if created_at:  # avoid waiting too long
                        alive_time = get_current_timestamp() - created_at
                        wait_time = int(depend.time) - alive_time
                        if wait_time > 0:
                            print(f"Wait in {wait_time}")
                            await asyncio.sleep(wait_time)
                    else:
                        await asyncio.sleep(int(depend.time))
                else:  # start and wait
                    await self.safe_start(depend.key)
                    await asyncio.sleep(int(depend.time))
        self.services[service].start()  # start after all

    async def start_all(self):
        self.is_running()
        for key in self.services.keys():
            await self.safe_start(key)
            await asyncio.sleep(float(settings.TIME_START_RELAX))

    def stop_all(self):
        for key, service in self.services.items():
            service.stop()

    def is_running(self):
        result: List[ServiceStatusResponse] = []
        for key, service in self.services.items():
            result.append(ServiceStatusResponse(
                code=service.info.key,
                translationCode=service.info.name,
                status=service.is_running(),
                pid=self.stateAdapter.data.services[service.info.key].pid,
                port=self.stateAdapter.data.services[service.info.key].port,
                state=self.stateAdapter.data.services[service.info.key].state,
                relation=service.info.relation,
            ))
        return result

    def provision(self):
        def safe_start(service):  # sync start local
            if self.stateAdapter.data.services[service].pid:  # already running:
                return
            if self.is_valid_dependencies:
                for dependency in self.services[service].info.relation.depend:
                    if self.stateAdapter.data.services[dependency.key].pid:  # already running
                        created_at = self.services[dependency.key].get_creationDate()
                        if created_at:  # avoid waiting too long
                            alive_time = get_current_timestamp() - created_at
                            wait_time = int(dependency.time) - alive_time
                            if wait_time > 0:
                                print(f"Wait in {wait_time}")
                                time.sleep(wait_time)
                        else:
                            time.sleep(int(dependency.time))
                    else:  # start and wait
                        safe_start(dependency.key)
                        time.sleep(int(dependency.time))
            self.services[service].start()  # start after all

        try:
            self.is_running()
            if self.systemAdapter.data.startup is True:  # startup
                for key in self.services.keys():
                    safe_start(key)
                    time.sleep(float(settings.TIME_START_RELAX))
        except CustomHTTPException as e:
            print(e.error_message)
        except:
            pass

        while True:  # provision thread
            try:
                self.is_running()  # check status for all services
                if self.stateAdapter.data.enable:
                    for key, run_time in self.stateAdapter.data.services.items():
                        if not run_time.pid and run_time.state is True:  # keep alive
                            safe_start(key)  # sure all dependencies are running
            except:
                import traceback
                traceback.print_exc()
                pass
            finally:
                time.sleep(int(settings.TIME_PROVISION))

if __name__ == "__main__":
    ServiceManager()
