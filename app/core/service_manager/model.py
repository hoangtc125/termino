from pydantic import BaseModel
from app.core.service.model import ServiceRelation


class ServiceStatusResponse(BaseModel):
    code: str
    translationCode: str
    status: bool
    pid: int = None
    port: int = None
    state: bool
    relation: ServiceRelation
