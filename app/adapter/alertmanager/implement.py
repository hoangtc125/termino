from typing import Dict

import yaml

from app.adapter.alertmanager.model import AlertManagerTemplateModel, TelegramConfig, TelegramReceiver
from app.infra.config import DASHBOARD_DIR
from app.adapter.alertmanager.interface import IAlertManagerTemplate
from app.lib import singleton


@singleton
class AlertManagerTemplate(IAlertManagerTemplate):
    def __init__(self):
        super().__init__()
        self.file_path = DASHBOARD_DIR + r'\resources\template\alertmanager.yaml'

        self.bootstrap()

    def bootstrap(self):
        with open(self.file_path, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            self.data: Dict = data

    def parse_data(self, data: AlertManagerTemplateModel):
        dict_data = data.dict()
        parse_data = {}
        for key, value in self.data.items():
            if any([f"(({field}))" in str(value) for field in dict_data.keys()]):
                parse_data[key] = dict_data[value[2:-2]]
            else:
                parse_data[key] = value
        return parse_data


if __name__ == "__main__":
    print(AlertManagerTemplate().parse_data(AlertManagerTemplateModel(
        receivers=[
            TelegramReceiver(
                telegram_configs=[
                    TelegramConfig(
                        bot_token="!23",
                        chat_id=-123,
                    )
                ]
            )
        ]
    )))
