from typing import Dict
from .model import AlertManagerTemplateModel


class IAlertManagerTemplate:
    def __init__(self):
        self.data: Dict = {}

    def bootstrap(self):
        raise NotImplementedError()

    def parse_data(self, data: AlertManagerTemplateModel):
        raise NotImplementedError()
