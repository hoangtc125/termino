from typing import List
from pydantic import BaseModel


class TelegramConfig(BaseModel):
    bot_token: str
    chat_id: int
    message: str = '{{ template "telegram.parkngo.message" . }}'


class TelegramReceiver(BaseModel):
    name: str = "telegram"
    telegram_configs: List[TelegramConfig]


class AlertManagerTemplateModel(BaseModel):
    receivers: List[TelegramReceiver]

