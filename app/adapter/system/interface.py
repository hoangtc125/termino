from .model import SystemAdapterModel


class ISystemAdapter:
    def __init__(self):
        self.data = SystemAdapterModel()

    def bootstrap(self):
        raise NotImplementedError()

    def update(self):
        raise NotImplementedError()

    def edit_language(self, language: str):
        raise NotImplementedError()
