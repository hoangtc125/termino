from pydantic import BaseModel


class SystemAdapterModel(BaseModel):
    language: str = "en"
