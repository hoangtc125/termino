import json

import yaml

from app.infra.config import DASHBOARD_DIR
from app.adapter.system.interface import ISystemAdapter
from app.adapter.system.model import SystemAdapterModel
from app.lib import singleton


@singleton
class SystemAdapter(ISystemAdapter):
    def __init__(self):
        super().__init__()
        self.file_path = DASHBOARD_DIR + r'\resources\system.yaml'

        self.bootstrap()

    def bootstrap(self):
        with open(self.file_path, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            self.data = SystemAdapterModel(**data)

    def update(self):
        json_data = self.data.json()
        dict_data = json.loads(json_data)
        with open(self.file_path, "w", encoding="utf8") as yaml_file:
            yaml.dump(dict_data, yaml_file, default_flow_style=False)

    def edit_language(self, language: str):
        self.data.language = language
        self.update()


if __name__ == "__main__":
    print(SystemAdapter().data)
