class IServiceAdapter:
    def __init__(self):
        self.data = {}

    def bootstrap(self):
        raise NotImplementedError()
