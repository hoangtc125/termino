from pyaml_env import parse_config

from app.infra.config import DASHBOARD_DIR
from app.adapter.service.interface import IServiceAdapter
from app.lib import singleton


@singleton
class ServiceAdapter(IServiceAdapter):
    def __init__(self):
        super().__init__()
        self.file_path = DASHBOARD_DIR + r'\resources\services.yaml'

        self.bootstrap()

    def bootstrap(self):
        self.data = parse_config(self.file_path)


if __name__ == "__main__":
    print(ServiceAdapter().data)
