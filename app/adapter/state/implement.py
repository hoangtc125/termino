import json

import yaml

from app.infra.config import DASHBOARD_DIR
from app.adapter.state.model import StateAdapterModel, StateService
from app.adapter.state.interface import IStateAdapter
from app.adapter.service import IServiceAdapter, ServiceAdapter
from app.lib import singleton


@singleton
class StateAdapter(IStateAdapter):
    def __init__(self, serviceAdapter: IServiceAdapter):
        super().__init__()
        self.serviceAdapter = serviceAdapter
        self.file_path = DASHBOARD_DIR + r'\resources\state.yaml'

        self.bootstrap()

    def bootstrap(self):
        with open(self.file_path, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
        if data:
            self.data.enable = data.get("enable", False)
            if isinstance(data.get("services"), dict):
                for key, service_state in data["services"].items():
                    self.data.services[key] = StateService(**service_state)
        if set(self.data.services.keys()) != set(self.serviceAdapter.data.keys()):
            for key, service in self.serviceAdapter.data.items():
                if key not in self.data.services:
                    self.data.services[key] = StateService()
            self.update()

    def update(self):
        json_data = self.data.json()
        dict_data = json.loads(json_data)
        with open(self.file_path, "w", encoding="utf8") as yaml_file:
            yaml.dump(dict_data, yaml_file, default_flow_style=False)

    def enable(self, service: str):
        self.data.services[service].state = True
        self.update()

    def disable(self, service: str):
        self.data.services[service].state = False
        self.update()

    def enable_all(self):
        for key in self.data.services.keys():
            self.data.services[key].state = True
        self.update()

    def disable_all(self):
        for key in self.data.services.keys():
            self.data.services[key].state = False
        self.update()

    def edit_service_pid(self, service: str, pid: int = None):
        self.data.services[service].pid = pid
        self.update()

    def edit_service_port(self, service: str, port: int = None):
        self.data.services[service].port = port
        self.update()


if __name__ == "__main__":
    print(StateAdapter(ServiceAdapter()).data)
