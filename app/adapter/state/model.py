from typing import Dict
from pydantic import BaseModel


class StateService(BaseModel):
    pid: int = None
    state: bool = False
    port: int = None


class StateAdapterModel(BaseModel):
    enable: bool = True
    services: Dict[str, StateService] = {}
