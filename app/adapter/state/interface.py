from .model import StateAdapterModel


class IStateAdapter:
    def __init__(self):
        self.data = StateAdapterModel()

    def bootstrap(self):
        raise NotImplementedError()

    def update(self):
        raise NotImplementedError()

    def disable(self, service: str):
        raise NotImplementedError()

    def enable(self, service: str):
        raise NotImplementedError()

    def enable_all(self):
        raise NotImplementedError()

    def disable_all(self):
        raise NotImplementedError()

    def edit_service_pid(self, service: str, pid: int = None):
        raise NotImplementedError()

    def edit_service_port(self, service: str, port: int = None):
        raise NotImplementedError()
