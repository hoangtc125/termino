from pydantic import BaseModel


class MonitorRuleTemplateModel(BaseModel):
    down_duration: str = "5m"
    high_memory_threshold: str = "15"
    high_memory_duration: str = "5m"
    low_disk_threshold: str = "15"
    low_disk_duration: str = "5m"
