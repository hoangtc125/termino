import copy
from typing import Dict

import yaml

from app.adapter.monitor_rule.model import MonitorRuleTemplateModel
from app.infra.config import DASHBOARD_DIR
from app.adapter.monitor_rule.interface import ImonitorRuleTemplate
from app.lib import singleton


@singleton
class MonitorRuleTemplate(ImonitorRuleTemplate):
    def __init__(self):
        super().__init__()
        self.file_path = DASHBOARD_DIR + r'\resources\template\monitor_rule.yaml'

        self.bootstrap()

    def bootstrap(self):
        with open(self.file_path, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            self.data: Dict = data

    def parse_data(self, data: MonitorRuleTemplateModel, parse_data=None):
        if not parse_data:
            parse_data = copy.deepcopy(self.data)
        if isinstance(parse_data, dict):
            for key, value in parse_data.items():
                if isinstance(value, dict) or isinstance(value, list):
                    parse_data[key] = self.parse_data(data, value)
                else:
                    dict_data = data.dict()
                    fields = [field for field in dict_data.keys() if f"(({field}))" in str(value)]
                    if fields:
                        for field in fields:
                            parse_data[key] = value.replace(f"(({field}))", dict_data[field])
                    else:
                        parse_data[key] = value
        elif isinstance(parse_data, list):
            for i, value in enumerate(parse_data):
                if isinstance(value, dict) or isinstance(value, list):
                    parse_data[i] = self.parse_data(data, value)
                else:
                    dict_data = data.dict()
                    fields = [field for field in dict_data.keys() if f"(({field}))" in str(value)]
                    if fields:
                        for field in fields:
                            parse_data[i] = value.replace(f"(({field}))", dict_data[field])
                    else:
                        parse_data[i] = value
        return parse_data


if __name__ == "__main__":
    print(MonitorRuleTemplate().parse_data(
        data=MonitorRuleTemplateModel(
            high_memory_threshold="34"
        )
    ))
