from typing import Dict
from .model import MonitorRuleTemplateModel


class ImonitorRuleTemplate:
    def __init__(self):
        self.data: Dict = {}

    def bootstrap(self):
        raise NotImplementedError()

    def parse_data(self, data: MonitorRuleTemplateModel):
        raise NotImplementedError()
