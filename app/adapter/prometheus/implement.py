import copy
from typing import Dict

import yaml

from app.adapter.prometheus.model import PrometheusTemplateModel
from app.infra.config import DASHBOARD_DIR
from app.adapter.prometheus.interface import IPrometheusTemplate
from app.lib import singleton


@singleton
class PrometheusTemplate(IPrometheusTemplate):
    def __init__(self):
        super().__init__()
        self.file_path = DASHBOARD_DIR + r'\resources\template\prometheus.yaml'

        self.bootstrap()

    def bootstrap(self):
        with open(self.file_path, "r", encoding="utf8") as yaml_file:
            data = yaml.safe_load(yaml_file)
            self.data: Dict = data

    def parse_data(self, data: PrometheusTemplateModel, parse_data=None):
        if not parse_data:
            parse_data = copy.deepcopy(self.data)
        if isinstance(parse_data, dict):
            for key, value in parse_data.items():
                if isinstance(value, dict) or isinstance(value, list):
                    parse_data[key] = self.parse_data(data, value)
                else:
                    dict_data = data.dict()
                    if any([f"(({field}))" in str(value) for field in dict_data.keys()]):
                        parse_data[key] = dict_data[value[2:-2]]
                    else:
                        parse_data[key] = value
        elif isinstance(parse_data, list):
            for i, value in enumerate(parse_data):
                if isinstance(value, dict) or isinstance(value, list):
                    parse_data[i] = self.parse_data(data, value)
                else:
                    dict_data = data.dict()
                    if any([f"(({field}))" in str(value) for field in dict_data.keys()]):
                        parse_data[i] = dict_data[value[2:-2]]
                    else:
                        parse_data[i] = value
        return parse_data


if __name__ == "__main__":
    print(PrometheusTemplate().parse_data(
        data=PrometheusTemplateModel(seed_hosts=["localhost:9182", "192.168.30.72:9182"])
    ))
