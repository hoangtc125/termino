from typing import Dict
from .model import PrometheusTemplateModel


class IPrometheusTemplate:
    def __init__(self):
        self.data: Dict = {}

    def bootstrap(self):
        raise NotImplementedError()

    def parse_data(self, data: PrometheusTemplateModel):
        raise NotImplementedError()
