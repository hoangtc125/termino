import re
from typing import List
from pydantic import BaseModel, root_validator
from app.infra.http_exc import CustomHTTPException


class PrometheusTemplateModel(BaseModel):
    seed_hosts: List[str]

    @root_validator
    def validate_host(cls, values):
        seed_hosts = values.get('seed_hosts')
        pattern = r'^(localhost|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|\[?[a-fA-F\d:]+\]?)\:(\d{1,5})$'
        for host in seed_hosts:
            if not re.match(pattern, host):
                raise CustomHTTPException(error_type="invalid_host")
        return values


if __name__ == "__main__":
    print(PrometheusTemplateModel(seed_hosts=["11.1.1.1:11111"]).dict())
