import re

def validate_host_port(string):
    pattern = r'^(localhost|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|\[?[a-fA-F\d:]+\]?)\:(\d{1,5})$'
    if re.match(pattern, string):
        return True
    else:
        return False

if __name__ == "__main__":

    # Sử dụng hàm
    print(validate_host_port("127.0.0:8080"))    # True
    print(validate_host_port("localhost:8080"))    # True
    print(validate_host_port("[::1]:8080"))        # True
    print(validate_host_port("example.com:8080"))  # True
    print(validate_host_port("invalid:8080"))      # False
