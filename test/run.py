import subprocess
import time
import os

alertmanager_path = r"C:\TSP\monitor_local\prometheus-2.45.1.windows-amd64"
os.chdir(alertmanager_path)
config_file = os.path.join(alertmanager_path, "alertmanager.yml")
args = os.path.join(alertmanager_path, "prometheus.exe")

if __name__ == "__main__":
    subprocess.Popen([args], shell=True)
    while True:
        time.sleep(1)
