import subprocess

path = "alertmanager"
cargs = (
    "wmic", "process", "where", f"ExecutablePath like \'%{path}%\'",
    "get", "ProcessId",
    "|", "findstr", "/v", "ProcessId",
    "|", "findstr", "/v", "wmic",
)
pid = subprocess.check_output(cargs, stderr=subprocess.PIPE, shell=True, encoding="utf-8").strip()

import re
import subprocess

cargs = (
    "netstat", "-ano",
    "|", "findstr", str(pid),
)
hosts = subprocess.check_output(cargs, stderr=subprocess.PIPE, shell=True, encoding="utf-8").strip()
pattern = re.compile(r":(\d+)")
ports = pattern.findall(hosts)

if __name__ == "__main__":
    print(pid, ports)
