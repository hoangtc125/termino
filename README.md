# ParknGo Backend Dashboard

## Prerequisites
- Must be in the folder which contains both `elasticsearch` and `application`
- Virtual environment folder name is:
    - **.venv** for python 64bit 
    - **.32venv** for python 32bit 

## Go to project
For both Ubuntu and Windows:
    ```
    cd be-tsp-dashboard
    ```

## Active virtual environment
- For Ubuntu
    ```
    source $PWD/.venv(.32venv)/bin/activate
    ```
- For Windows 10/11:
    ```
    %cd%/.venv(.32venv)/Scripts/activate
    ```
## Install dependencies
    pip install -r requirements.txt
## Run

- Checkout branch standart
```
git checkout origin standart
```
- Run
    - for Ubuntu

        ```
        set PYTHONPATH=$PWD
        python app\application.py
        ```
    - for Windows

        ```
        set PYTHONPATH=%cd%
        python app\application.py
        ```
- Call API from IP: http://localhost:8010
- Check list API (Swagger): http://localhost:8010/docs

## Build
- Copy script 1 in `be-tsp-dashboard\pyinstaller.txt` and run in terminal
- Change folder/file paths to compatible with your project location
- Output folder name is located in `be-tsp-dashboard\dist` which name is same `be-tsp-dashboard`

### Note:

Default password: admin1